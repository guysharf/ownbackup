from opensearchpy import OpenSearch


class OpenSearchClient:
    def __init__(self, host, port, auth):
        self.client = OpenSearch(
                                hosts=[{'host': host, 'port': port}],
                                http_compress=True,
                                http_auth=auth,
                                use_ssl=False,
                                verify_certs=False,
                                ssl_assert_hostname=False,
                                ssl_show_warn=False
                        )

    def create_index(self, iname):
        # Check if exists
        if self.client.indices.exists(iname):
            print(f"Index {iname} already exists")
            return
        body = {
                'settings': {
                    'index': {
                        'number_of_shards': 4
                        }
                    }
                }
        res = self.client.indices.create(iname, body=body)
        print(f"Created index {iname}:\n {res}")

    def add_doc(self, doc, iname, doc_id):
        if type(doc) != dict:
            print(f"Cannot send type {type(doc)}")
            return
        res = self.client.index(
            index=iname,
            body=doc,
            id=doc_id,
            refresh=True
        )
        print(f"Added doc #{doc_id} to {iname}: \n {res}")

    def search(self, query, iname, *fields, limit=5):
        body = {
                'size': limit,
                'query': {
                    'multi_match': {
                        'query': query,
                        'fields': [*fields]
                    }
                }
        }
        res = self.client.search(body=body, index=iname)
        print(f"Found results: \n{res}")

    def delete_doc(self, iname, doc_id):
        res = self.client.delete(index=iname, id=doc_id)
        print(f"Deleted document #{doc_id}:\n{res}")
