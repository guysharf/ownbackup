import psutil


def get_cpu_times():
    times = psutil.cpu_times()
    return dict(times._asdict())


def get_mem():
    res = psutil.virtual_memory()
    return dict(res._asdict())


def get_loadavg(percent=False):
    one, five, fifteen = psutil.getloadavg()
    averages = {"1": one,
                "5": five,
                "15": fifteen
                }
    if percent:
        count = psutil.cpu_count()
        for k, v in averages.items():
            averages[k] = v / count * 100
    return averages


def get_disks_partitions(physical=True):
    res = psutil.disk_partitions(all=not physical)
    disks = []
    for item in res:
        disks.append(dict(item._asdict()))
    return disks
