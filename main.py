import sensors
from opensearch import OpenSearchClient
import time


if __name__ == "__main__":
    client = OpenSearchClient("localhost", 9200, ('admin', 'admin'))
    iname = "performance"
    client.create_index(iname)
    doc_ids = []
    for i in range(5):
        doc = {
            "load%": sensors.get_loadavg(True),
            "load": sensors.get_loadavg(),
            "cputime": sensors.get_cpu_times(),
            "mem": sensors.get_mem(),
            "timestamp": int(time.time())
        }
        for disk in sensors.get_disks_partitions():
            doc.update(disk)
        doc_id = doc["timestamp"]
        doc_ids.append(doc_id)
        client.add_doc(doc, iname, doc_id)
        time.sleep(10)

    #  Display all results
    for doc_id in doc_ids:
        client.search(doc_id, iname, "timestamp")

#  todo: Client: add more error handling for all operations
#  todo: Dynamic IDs instead of timestamps
#  todo: User arguments
#  todo: Add more sensors, add more flexibility to existing sensors.
#  todo: Custom logging
